<% local view, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#listsubnets").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"editsubnet", "delsubnet", "createsubnet"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="listsubnets" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Subnet</th>
	</tr>
</thead><tbody>
<% local subn = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,subnet in ipairs(view.value) do %>
	<tr>
		<td>
			<%
			subn.value = subnet
			htmlviewfunctions.displayitem(cfe({type="link", value={subnet=subn, redir=redir}, label="", option="Edit", action="editsubnet"}), page_info, -1)
			htmlviewfunctions.displayitem(cfe({type="form", value={subnet=subn}, label="", option="Delete", action="delsubnet" }), page_info, -1)
			%>
		</td>
		<td><%= html.html_escape(subnet) %></td>
	</tr>
<% end %>
</tbody></table>

<% htmlviewfunctions.displayitem(cfe({type="link", value={redir=redir}, label="Add new subnet", option="New", action="createsubnet"}), page_info, 0) %>

<% htmlviewfunctions.displaysectionend(header_level) %>
