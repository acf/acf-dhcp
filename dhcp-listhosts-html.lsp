<% local view, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>
<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#listhosts").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>


<% htmlviewfunctions.displaycommandresults({"edithost", "delhost", "createhost"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="listhosts" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Host</th>
	</tr>
</thead><tbody>
<% local hst = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,host in ipairs(view.value) do %>
	<tr>
		<td>
			<%
			hst.value = host
			htmlviewfunctions.displayitem(cfe({type="link", value={host=hst, redir=redir}, label="", option="Edit", action="edithost"}), page_info, -1)
			htmlviewfunctions.displayitem(cfe({type="form", value={host=hst}, label="", option="Delete", action="delhost" }), page_info, -1)
			%>
		</td>
		<td><%= html.html_escape(host) %></td>
	</tr>
<% end %>
</tbody></table>

<% htmlviewfunctions.displayitem(cfe({type="link", value={redir=redir}, label="Add new host", option="New", action="createhost"}), page_info, 0) %>

<% htmlviewfunctions.displaysectionend(header_level) %>
