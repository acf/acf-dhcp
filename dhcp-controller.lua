-- the dhcpd  controller
local mymodule = {}

mymodule.default_action = "status"

mymodule.status = function ( self )
	return self.model.getstatus()
end

mymodule.startstop = function ( self )
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

mymodule.settings = function( self )
	return self.handle_form(self, self.model.read_settings, self.model.update_settings, self.clientdata, "Save", "Update Global Settings", "Global Settings Updated")
end

mymodule.editsubnet = function ( self )
	return self.handle_form(self, self.model.subnet_read, self.model.subnet_update, self.clientdata, "Save", "Edit Subnet", "Subnet Settings Updated")
end

mymodule.createsubnet = function ( self )
	return self.handle_form(self, self.model.create_new_subnet, self.model.subnet_create, self.clientdata, "Create", "Create new subnet", "New subnet Created")
end

mymodule.delsubnet = function(self)
	return self.handle_form(self, self.model.get_subnet_delete, self.model.subnet_delete, self.clientdata, "Delete", "Delete subnet", "Subnet Deleted")
end

mymodule.listsubnets = function ( self )
	return self.model.get_subnets()
end

mymodule.edithost = function ( self )
	return self.handle_form(self, self.model.host_read, self.model.host_update, self.clientdata, "Save", "Edit Host", "Host Settings Updated")
end

mymodule.createhost = function ( self )
	return self.handle_form(self, self.model.create_new_host, self.model.host_create, self.clientdata, "Create", "Create new host", "New host Created")
end

mymodule.delhost = function(self)
	return self.handle_form(self, self.model.get_host_delete, self.model.host_delete, self.clientdata, "Delete", "Delete host", "Host Deleted")
end

mymodule.listhosts = function ( self )
	return self.model.get_hosts()
end

mymodule.viewleases = function ( self )
	return self.model.getleases()
end

mymodule.listfiles = function(self)
	return self.model.listconfigfiles()
end

mymodule.expert = function(self)
	return self.handle_form(self, self.model.getconfigfile, self.model.setconfigfile, self.clientdata, "Save", "Edit DHCP File", "File Saved")
end

return mymodule
