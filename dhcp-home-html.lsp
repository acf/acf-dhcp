<% local view, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% htmlviewfunctions.displaycommandresults({"settings", "edithost", "delhost", "createhost", "editsubnet", "delsubnet", "createsubnet"}, session) %>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("status")
end %>

<%
local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Global Settings"}), page_info)
htmlviewfunctions.displayitem(cfe({type="link", value={redir=cfe({ type="hidden", value=page_info.orig_action })}, label="Edit global settings", option="Edit", action="settings"}), page_info, 0)
htmlviewfunctions.displaysectionend(header_level)
%>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("listsubnets")
end %>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("listhosts")
end %>
